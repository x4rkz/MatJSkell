# MatJSkell

## Qu'est-ce que c'est ?

Mathskell est un autre projet qui stocke des résultats tels que des théorèmes, des définitions. Imaginez que vous voudriez citer un tel résultat sur un site, un forum. L'idéal serait de taper `\mathskell{id}` où `id` serait l'id du résultat en question, et qu'il soit remplacé par le code html/mathjax du résultat.

## Installation

Dans votre header html, il faut charger le script.

    <script src="http://localhost:3000/MatJSkell/MatJSkell.js" type="text/javascript"></script> 
  
Suivi de ce code minimal.

    <script type="text/javascript">MatHSkell.className = "latex";</script>

Et c'est tout ! (Verifier qu'il y a bien mathjax...)