var className = "latex";

var objects = document.getElementsByClassName(className);
var re = new RegExp('\\\\mathskell{(ID)}', 'g');

for (var i=0, l=objects.length; i<l; i++) { 
  objects[i].innerHTML = objects[i].innerHTML.replace(re, 'CONTENT');

}